﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Net;
using System.Xml.Linq;
using System.IO;

namespace Camera_Recorder
{
    public partial class Form1 : Form
    {
        public static class MyGlobals
        {
            public static string[] recordingPath = new string[2];
            public static string user = ConfigurationManager.AppSettings["user"];
            public static string pass = ConfigurationManager.AppSettings["pass"];
            public static string IPAddressesConf = ConfigurationManager.AppSettings["IpAddress"];
            public static string PortsConf = ConfigurationManager.AppSettings["Port"];
            public static string SDCardsConf = ConfigurationManager.AppSettings["SDCard"];
        }
        public Form1()
        {
            InitializeComponent();
            
        }
        public static HttpWebResponse ExecuteRequest(string requestUrl, NetworkCredential AxisCredential)
        {
            WebRequest request = WebRequest.Create(requestUrl);
            request.Credentials = AxisCredential;
            return (HttpWebResponse)request.GetResponse();
        }
        private void playBtn_Click(object sender, EventArgs e)
        {
            

            string[] IPAddresses = MyGlobals.IPAddressesConf.Split(',');
            string[] Ports = MyGlobals.PortsConf.Split(',');
            string[] SDCards = MyGlobals.SDCardsConf.Split(',');

            try
            {
                for (int i = 0; i < IPAddresses.Length; i++)
                {
                    var _startRecoringUrl = String.Format("http://{0}:{1}/axis-cgi/record/record.cgi?diskid={2}",
                                IPAddresses[i].Trim(), Ports[i].Trim(), SDCards[i].Trim());
                    logTexbox.AppendText(String.Format("Attempting to start recording on  camera {0}", Ports[i]));
                    logTexbox.AppendText(Environment.NewLine);
                    var response = ExecuteRequest(_startRecoringUrl, new NetworkCredential(MyGlobals.user, MyGlobals.pass));
                    XDocument doc;
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        doc = XDocument.Load(responseStream);
                    }
                    MyGlobals.recordingPath[i] = doc.Root.Element("record").Attribute("recordingid").Value;
                    logTexbox.AppendText(String.Format("Request sent to {0} , to start recording", MyGlobals.recordingPath[i]));
                    logTexbox.AppendText(Environment.NewLine);
                    
                    
                    playBtn.Enabled = false;
                }
                logTexbox.AppendText("---------------------RECORDING STARTS----------------------");
                logTexbox.AppendText(Environment.NewLine);
            }
            catch(Exception ex)
            {
                logTexbox.AppendText("Unable to start recording: "+ex.Message);
                Console.WriteLine(ex);
            }
            
        }

        private void stopBtn_Click(object sender, EventArgs e)
        {

            //logTexbox.AppendText("recording stopped");
            StopSessionRecording(logTexbox);
            DownloadRecording(logTexbox);
            MyGlobals.recordingPath = null;
            playBtn.Enabled = true;
            logTexbox.AppendText("---------------------RECORDING ENDS----------------------");
            logTexbox.AppendText(Environment.NewLine);

        }
        public static void StopSessionRecording(TextBox logTexbox)
        {

            string[] IPAddresses = MyGlobals.IPAddressesConf.Split(',');
            string[] Ports = MyGlobals.PortsConf.Split(',');
            string[] SDCards = MyGlobals.SDCardsConf.Split(',');
            if (MyGlobals.recordingPath != null)
            {
                for (int i = 0; i < IPAddresses.Length; i++)
                {
                    var _stopRecoringUrl = String.Format("http://{0}:{1}/axis-cgi/record/stop.cgi?recordingid={2}",
                            IPAddresses[i], Ports[i], MyGlobals.recordingPath[i]);
                    
                    logTexbox.AppendText(String.Format("Attempting to stop recording on camera {0}", Ports[i]));
                    logTexbox.AppendText(Environment.NewLine);
                    var response = ExecuteRequest(_stopRecoringUrl, new NetworkCredential(MyGlobals.user, MyGlobals.pass));
                    //TODO properly handle this respnse
                    response.Close();
                    logTexbox.AppendText(String.Format("Request sent to {0} , to stop recording", Ports[i]));
                    logTexbox.AppendText(Environment.NewLine);
                }
                    
            }
           
        }

        public static void DownloadRecording(TextBox logTexbox)
        {
            string[] IPAddresses = MyGlobals.IPAddressesConf.Split(',');
            string[] Ports = MyGlobals.PortsConf.Split(',');
            string[] SDCards = MyGlobals.SDCardsConf.Split(',');
            DateTime now = DateTime.Now;
            string dateas = now.ToString("MM-dd-yy");
            for (int i = 0; i < IPAddresses.Length; i++)
            {
                var _exportRecoringUrl = String.Format("http://{0}:{1}/axis-cgi/record/export/exportrecording.cgi?schemaversion=1&recordingid={2}&diskid={3}&exportformat=matroska", IPAddresses[i], Ports[i], MyGlobals.recordingPath[i], SDCards[i]);
                var pathString = Path.Combine(@"C:\Recording\", dateas);
                var x = Directory.CreateDirectory(pathString);
                string sourceUrl = _exportRecoringUrl;
                var pathSaveFile = pathString + "\\" + MyGlobals.recordingPath[i]+ ".mkv";
                string targetdownloadedFile = pathSaveFile;
                logTexbox.AppendText(String.Format("Start downloading of Recording {0} Finished", Ports[i]));
                logTexbox.AppendText(Environment.NewLine);
                DownloadManager.DownloadFile(sourceUrl, targetdownloadedFile, new NetworkCredential(MyGlobals.user, MyGlobals.pass));
                logTexbox.AppendText(String.Format("Download of Recording {0} Finished", Ports[i]));
                logTexbox.AppendText(Environment.NewLine);
            }
                
        }
    }
}
